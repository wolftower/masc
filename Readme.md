Multi-Assignment Clustering
===========================

Multi-Assignment Clustering (MAsC) is a extension of k-means clustering
that allows data points to be assigned to multiple clusters
simultaneously. The method takes a matrix of continuous data as input
and first performs k-means clustering to obtain k centroids. The
distances between all data points and the centroids are then calculated
and transformed into probabilities. A user-defined threshold lambda is
then applied to each data point, and it is assigned to all clusters
where the probability of cluster membership is greater than the
threshold.

A tutorial for geting started with MAsC is available
[here](https://wolftower.gitlab.io/masc/).

Installation
============

MAsC can be installed by running the following commands in R.

``` r
if(!require(devtools)) install.packages("devtools")
devtools::install_git(url = "https://gitlab.com/wolftower/masc.git")
```
